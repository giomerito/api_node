const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const app = express();


const produtosRouter = require('./routes/produtos');
const pedidosRouter = require('./routes/pedidos');
const usuariosRouter = require('./routes/usuarios');

//Mostra os logs das rotas acessadas
app.use(morgan("dev"));
app.use('/upload', express.static('upload'));
app.use(bodyParser.urlencoded({ extended: false })); //apenas dados simples
app.use(bodyParser.json()); //aceita formato de entrada como json

//Definição das configurações de CORS
app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header(
		"Access-Control-Allow-Header", 
		"Origin, x-Requested-With, Content-Type, Accept, Authorization"
	);
	if(req.method === 'OPTIONS'){
		res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
		return res.status(200).send({});
	}
	next();
});

app.use('/produtos', produtosRouter);
app.use('/pedidos', pedidosRouter);
app.use('/usuarios', usuariosRouter);

//Quando não encontrar rota passa por este
app.use((req, res, next) => {
	const erro = new Error("Not Found");
	erro.status = 404;
	next(erro);
});

app.use((error, req, res, next) => {
	res.status(error.status || 500);
	return res.send({
		message: error.message
	});
});

//Rota padrão do sistema
app.use("/", (req, res, next) => {
	res.status(200).send({
		message: "OK, Deu certo"
	});
});

module.exports = app;