const router = require('express').Router();
const mysql = require('../db/db_mysql').pool;

const multer = require('multer');
const storage = multer.diskStorage({
	destination: function(req, file, callback){
		callback(null, './upload/');
	},
	filename: function(req, file, callback){
		callback(null, new Date().toISOString() + file.originalname);
	}
});
const fileFilter = (req, file, callback) => {
	if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
		callback(null, true);
	}else{
		callback(null, true);
	}
}
const upload = multer({ 
	storage: storage,
	limits: {
		fileSize: 1024 * 1024 * 5		
	},
	fileFilter: fileFilter 
});

router.get('/', (req, res) => {
	mysql.getConnection((error, conn) => {
		if(error){ return res.status(500).send({ error: error }) }
		conn.query(
			"SELECT * FROM tb_produtos",
			(err, result, fields)=>{
				conn.release();
				if(err){res.status(500).send({error: err});}
				//Aprimorando o retorno
				const response = {
					quantidade: result.length,
					produtos: result.map(prod => {
						return {
							prod_id: prod.prod_id,
							nome: prod.prod_nome,
							valor: prod.prod_valor,
							imagem: prod.prod_image,
							detalhes: {
								tipo: "GET",
								descricao: "Retorna todos os produtos",
								url: "http://localhost:3000/produtos/" + prod.prod_id
							}
						}
					})
				}
				res.status(200).send({produtos: response});
			}
		);	
	});
});

router.get('/:prod_id', (req, res) => {
	const id = req.params.prod_id;
	mysql.getConnection((error, conn) => {
		if(error){ return res.status(500).send({ error: error }) }
		conn.query(
			"SELECT * FROM tb_produtos WHERE prod_id = ?", [id],
			(err, result, fields)=>{
				conn.release();
				if(err){res.status(500).send({error: err});}	
				if(result.length != 1){
					return res.status(404).send({
						message: "Produto não encontrado",
						status: "404 Not Found"
					});
				}
				const response = {
					produto: {
						prod_id: result[0].prod_id,
						nome: result[0].prod_nome,
						valor: result[0].prod_valor,
						imagem: result[0].prod_image,
						detalhes: {
							tipo: "GET",
							descricao: "Retorna um produto conforme id informado",
							url: "http://localhost:3000/produtos/" + result[0].prod_id
						}
					}
				}		
				res.status(200).send(response);
			}
		);	
	});
});

router.post('/', (upload.single('produto_imagem')), (req, res) => {	
	const { prod_nome, prod_valor } = req.body;
	const prod_image = req.file.path;
	mysql.getConnection((error, conn) => {
		if(error){ return res.status(500).send({ error: error }) }
		conn.query(`INSERT INTO tb_produtos(prod_nome, prod_valor, prod_image)
			VALUES(?, ?, ?)`, 
			[prod_nome, prod_valor, prod_image],
			(err, result, fields) => {
				conn.release();
				if(err){res.status(500).send({error: err});}
				//Aprimorando o retorno
				const response = {
					message: "Produto adicionado",
					produto: {
						prod_id: result.insertId,
						nome: prod_nome,
						valor: prod_valor,
						image: prod_image,
						detalhes: {
							tipo: "POST",
							descricao: "Adiciona um produto",
							url: "http://localhost:3000/produtos/" + result.insertId
						}
					}					
				}
				res.status(201).send(response);
			}
		);	
	});	
});

router.patch('/:prod_id', (req, res) => {
	const id = req.params.prod_id;
	const { prod_nome, prod_valor } = req.body;
	mysql.getConnection((error, conn) => {
		if(error){ return res.status(500).send({ error: error }) }
		conn.query(`UPDATE tb_produtos SET prod_nome = ?, prod_valor = ? WHERE prod_id = ?`, 
			[prod_nome, prod_valor, id],
			(err, result, field) => {
				conn.release();
				if(err){res.status(500).send({error: err});}					
				//Aprimorando o retorno
				const response = {
					message: "Produto alterado",
					produto: {
						prod_id: id,
						nome: prod_nome,
						valor: prod_valor,
						request: {
							tipo: "PATCH",
							descricao: "Altera os dados de um produto",
							url: "http://localhost:3000/produtos/" + id
						}
					}					
				}
				res.status(201).send(response);
			}
		);	
	});	
});

router.delete('/:prod_id', (req, res) => {
	const id = req.params.prod_id;
	mysql.getConnection((error, conn) => {
		conn.query('DELETE FROM tb_produtos WHERE prod_id = ?', [id],
		(err, result, fields) => {
			if(err){
				if(err){res.status(500).send({error: err});}
			}
			const response = {
				message: "Produto deletado",
				detalhes: {
					tipo: "DELETE",
					descricao: "Deleta um produto conforme ID informado",
					url: "http://localhost:3000/produtos"
				}
			}
			res.status(202).send(response);
		});
	});	
});

module.exports = router;