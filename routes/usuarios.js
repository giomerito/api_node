const router = require('express').Router();
const mysql = require('../db/db_mysql').pool;
const bcrypt = require('bcrypt');

router.get('/', (req, res) => {
	mysql.getConnection((error, conn) => {
		if(error){ return res.status(500).send({ error: error });}
		conn.query("SELECT * FROM tb_usuarios", (err, result) => {
			conn.release();
			if(err){ return res.status(500).send({ error: err });}
			const response = {
				quantidade: result.length,
				usuarios: result.map(usu => {
					return {
						id: usu.usu_id,
						nome: usu.usu_nome,
						email: usu.usu_email,
						detalhes: {
							tipo: "GET",
							descricao: "Retorna todos os usuarios",
							url: "http://localhost:3000/usuarios/" + usu.usu_id
						}
					}
				})
			}	
			return res.status(200).send({usuarios: response});
		});
	});
});

router.post('/cadastro', (req, res) => {
	const { usu_nome, usu_email, usu_senha } = req.body;
	mysql.getConnection((error, conn) => {
		if(error){return res.status(500).send({ error: error });}

		conn.query("SELECT usu_email FROM tb_usuarios WHERE usu_email = ?", [usu_email], (err, result) => {
			if(result.length > 0){
				res.status(409).send({ message: "Usuário já está cadastrado"});
			}else{
				bcrypt.hash(usu_senha, 10, (errBcrypt, hash) => {
					if(errBcrypt){return res.status(500).send({error: errBcrypt});}
					conn.query("INSERT INTO tb_usuarios(usu_nome, usu_email, usu_senha) VALUES(?, ?, ?);",
						[usu_nome, usu_email, hash], 
						(err, result) => {
							conn.release();
							if(err){return res.status(500).send({ error: err });}
							const response = {
								message: "Usuário criado com sucesso",
								usuario: {
									id: result.insertId,
									nome: usu_nome,
									email: usu_email
								}
							}
							return res.status(201).send({usuario: response});
						}
					);
				});
			}
		});
	});
});

router.post('/login', (req, res) => {
	const { usu_email, usu_senha } = req.body;
	mysql.getConnection((error, conn) => {
		if(error){return res.status(500).send({ error: error });}

		conn.query("SELECT * FROM tb_usuarios WHERE usu_email = ?", 
			[usu_email], 
			(err, result) => {
				conn.release();
				if(err){return res.status(500).send({ error: err });}
				if(result.length < 1){
					return res.status(401).send({message: "Falha na autenticação"});
				}
				bcrypt.compare(usu_senha, result[0].usu_senha, (er, results) => {
					if(er){
						return res.status(401).send({message: "Falha na autenticação"});
					}
					if(results){
						return res.status(200).send({message: "Autenticado com sucesso"});
					}
					return res.status(401).send({message: "Falha na autenticação"});
				})
		})
	})
});

module.exports = router;