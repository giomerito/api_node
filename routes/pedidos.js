const { Router } = require('express');
const mysql = require("../db/db_mysql").pool;
const router = Router();

//Adicionando um ou mais produtos no pedido
router.post('/add', (req, res) => {
	const { cliente_id, item } = req.body;
	mysql.getConnection((error, conn) => {
		if(error){return res.status(500).send({ error: error });}
		
		conn.query("INSERT INTO tb_pedidos(cliente_id) values(?);", [cliente_id], 
			(err, result) => {			
			if(err){return res.status(500).send({ error: err });}
			
			for (var i = 0; i < item.length; i++) {
				conn.query("INSERT INTO tb_itensPedido(pedido_id, item, produto_id, quantidade) values(?, ?, ?, ?);",
					[result.insertId, item[i].item, item[i].produto_id, item[i].quantidade], 
					(itemError, itemResult) => {
						conn.release();
						if(itemError){return res.status(500).send({ error: itemError });}
				});
			}
			return res.status(201).send({ 
				message: "Pedido adicionado com sucesso",
				pedido: result.insertId,
				items: item
			}); 
		});

	});//Fim da getConnection			
});

router.get('/lista/:id', (req, res) => {
	const id = req.params.id;
	mysql.getConnection((error, conn) => {
		if(error){ return res.status(500).send({ error: error });}
		conn.query(
			`select ped_id, cli_nome from tb_pedidos as p
			inner join tb_clientes as c
			on p.cliente_id = c.cli_id where ped_id = ?`, [id],
			(err, result) => {
				if(err){ return res.status(500).send({ error: err });}

				if(result.length != 1 || result.length == 0){
					return res.status(404).send({message: "Pedido não existe"}); 
				}
				conn.query(
					`select item, prod_id, prod_nome, prod_valor, prod_image, 
					quantidade from tb_itensPedido as i
					inner join tb_produtos as p
					on i.produto_id = p.prod_id 
					where i.pedido_id = ?`, [id],
					(iErr, iResult) => {
						conn.release();
						if(iErr){ return res.status(500).send({ error: iErr });}
						const response = {
							pedido: {
								numero: result[0].ped_id,
								cliente: result[0].cli_nome,
								itens: iResult.map(item => {
									const totalItem = item.prod_valor * item.quantidade;
									const total = 0; 
									return {
										item: item.item,
										codigo: item.prod_id,
										descricao: item.prod_nome,
										valor: item.prod_valor,
										qtd: item.quantidade,
										totalItem: totalItem, 
									}
								})
							}							
						}
						return res.status(202).send(response); 
					}
				)
			}
		);
	});
});

module.exports = router;